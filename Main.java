package com.no.se.veremos.que.onda;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        FileSystemHandler fileSystemHandle = new FileSystemHandler();
        Scanner scanner = new Scanner(System.in);
        String command  = scanner.next();
        fileSystemHandle.executeCommand(command);
        scanner.close();

    }


}
