package com.no.se.veremos.que.onda;

import java.util.*;

public class FileSystemHandler {
    private CustomFile currentCustomFile;

    public FileSystemHandler(){
        currentCustomFile = new CustomFile("/root", null, new HashSet<CustomFile>());
    }


    public void executeCommand(String command){
        // I would like to simplify this with a map of <String, Consumer> but compilation as java 1.5 don't let me do it
        if("pwd".equalsIgnoreCase(command)){
            System.out.println(buildPath());
        } else if("ls".equalsIgnoreCase(command)){
            Iterator<CustomFile> customFileIterator = currentCustomFile.getChilds().iterator();
            while(customFileIterator.hasNext()){
                System.out.println(customFileIterator.next().getFileName());
            }
        } else if("quit".equalsIgnoreCase(command)){
            System.exit(0);
        }
        else if("mkdir".startsWith(command.toLowerCase())){
            String[] splittedCommand = command.split(" ");
            currentCustomFile.getChilds().add(new CustomFile("/"
                    + splittedCommand[1], null, new HashSet<CustomFile>()));
        }
        else if("cd".startsWith(command.toLowerCase())){
            String[] splittedCommand = command.split(" ");
            Optional<CustomFile> customFileOptional = findFile(splittedCommand[1]);
            if(customFileOptional.isPresent()){
                if(splittedCommand[1].equals("..")){
                    if(currentCustomFile.getParent() != null){
                        currentCustomFile = currentCustomFile.getParent();
                    }
                } else{
                    currentCustomFile = customFileOptional.get();
                }

            } else{
                System.out.println("Directory Not Found");
            }
        }
        else if("touch".startsWith(command.toLowerCase())){
            String[] splittedCommand = command.split(" ");
            Optional<CustomFile> customFileOptional = findFile(splittedCommand[1]);
            if(customFileOptional.isPresent()){
                System.out.println("File Already Exists");
            } else{
                currentCustomFile.getChilds().add(new CustomFile(splittedCommand[1], null, null)) ;
            }
        }
        else{
            System.out.println("Unrecognized command");
        }
    }

    private Optional<CustomFile> findFile(String fileName){
        Iterator<CustomFile> iterator = currentCustomFile.getChilds().iterator();
        while(iterator.hasNext()){
            CustomFile cf = iterator.next();
            if(fileName.equals(cf.getFileName())){
                return Optional.ofNullable(cf);
            }
        }
        return Optional.empty();
    }

    private String buildPath(){
        List<String> path = new ArrayList<String>();
        path.add(currentCustomFile.getFileName());
        CustomFile parent = currentCustomFile.getParent();
        while(parent != null){
            path.add(parent.getFileName());
            parent = parent.getParent();
        }
        Collections.reverse(path);
        return String.join("/", path);
    }
}
