package com.no.se.veremos.que.onda;

import java.util.Set;

public class CustomFile {
    private String fileName;
    private CustomFile parent;
    private Set<CustomFile> childs;

    public CustomFile(String fileName, CustomFile parent, Set<CustomFile> childs){
        this.fileName = fileName;
        this.parent = parent;
        this.childs = childs;
    }

    public String getFileName(){
        return this.fileName;
    }

    public CustomFile getParent(){
        return this.parent;
    }

    public Set<CustomFile> getChilds(){
        return this.childs;
    }
}
